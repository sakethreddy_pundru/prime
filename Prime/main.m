//
//  main.m
//  Prime
//
//  Created by admin on 25/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
