//
//  AppDelegate.h
//  Prime
//
//  Created by admin on 25/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

